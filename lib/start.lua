local start = {}

local colors = require "lib.colors"
local bit8 = require "lib.fonts".bit8

start.mouseX=0
start.mouseY=0

local gap = 6
start.active = true
start.first = true

function start:draw1(w,h)	
	--love.graphics.setColor(colors.startbg)
	--love.graphics.rectangle("fill",0,0,w,h)
	local st = ""
	
	
	if self.first then
		st = "start"
	else
		st = "resume"
	end

	local textW = bit8:getWidth(st)
	local textH = bit8:getHeight(st)
	
	local bw = 2*w/3
	local bh = textH + 10
	local bx = w/6
	local by = h/2 - gap/2 - bh
	
	-- button start
	if (self.mouseX > bx and self.mouseX < bx+bw)and(self.mouseY > by and self.mouseY < by+bh)  then
		love.graphics.setColor(unpack(colors.btnhover))
		love.graphics.rectangle("fill",bx,by,bw,bh)
		love.graphics.setColor(unpack(colors.texthover))
		love.graphics.print(st,bit8,bx+(bw-textW)/2,by+5)
	else	
		love.graphics.setColor(unpack(colors.btn))
		love.graphics.rectangle("fill",bx,by,bw,bh)
		love.graphics.setColor(unpack(colors.text))
		love.graphics.print(st,bit8,bx+(bw-textW)/2,by+5)
	end

	textW = bit8:getWidth("exit")
	textH = bit8:getHeight("exit")
	bx = w/6
	by = h/2 + gap/2
	bh = textH+10
	bw = 2*w/3

	-- button exit
	if (self.mouseX > bx and self.mouseX < bx+bw) and (self.mouseY > by and self.mouseY < by+bh) then
		love.graphics.setColor(unpack(colors.btnhover))
		love.graphics.rectangle("fill",bx,by,bw,bh)
		love.graphics.setColor(colors.texthover)
		love.graphics.print("exit",bit8,bx+(bw-textW)/2,by+5)
	else	
		love.graphics.setColor(unpack(colors.btn))
		love.graphics.rectangle("fill",bx,by,bw,bh)
		love.graphics.setColor(colors.text)
		love.graphics.print("exit",bit8,bx+(bw-textW)/2,by+5)
	end
end

function start.exit()
	love.event.quit(0)
end
function start:mousepressed(x,y,w,h)

	local textH = bit8:getHeight("start")
	local bw = 2*w/3
	local bh = textH + 10
	local bx = w/6
	local by = h/2 - gap/2 - bh
	
	if (x > bx and x < bx + bw) and (y > by and y < by + bh) then
		--print("start")
		self.active = false
	else
		by = h/2 + gap/2
		if (x > bx and x < bx + bw) and (y > by and y < by + bh) then
			self.exit()
		end
	end
end
return start
