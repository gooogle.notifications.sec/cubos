local world = {}
local gravity = 200
local can_bodies_sleep = true
local bit8 = require "lib.fonts".bit8
local colors = require "lib.colors"

world.world = love.physics.newWorld(0,gravity,can_bodies_sleep)

-- geometry of the world
world.geo = {}
function world:geoLoad(x,y)
	local floor = {}
	floor.body = love.physics.newBody(self.world,x/2,y-y/6-50,"static")
	floor.shape = love.physics.newRectangleShape(x*2/3,100)
	floor.fixture = love.physics.newFixture(floor.body,floor.shape)
	floor.fixture:setFriction(.5)
	table.insert(self.geo,floor)

	local wall_1 = {}
	wall_1.body = love.physics.newBody(self.world,x/6 + 25,34*y/48 - 100,"static")
	wall_1.shape = love.physics.newRectangleShape(50,y/4)
	wall_1.fixture = love.physics.newFixture(wall_1.body,wall_1.shape)
	wall_1.fixture:setFriction(.5)
	table.insert(self.geo,wall_1)

	local wall_2 = {}
	wall_2.body = love.physics.newBody(self.world,5*x/6-25,34*y/48 - 100,"static")
	wall_2.shape = love.physics.newRectangleShape(50,y/4)
	wall_2.fixture = love.physics.newFixture(wall_2.body,wall_2.shape)
	wall_2.fixture:setFriction(.5)
	table.insert(self.geo,wall_2)
	
	local cube = {}
	cube.body = love.physics.newBody(self.world,x/2,85)
	cube.shape = love.physics.newRectangleShape(50,50)
	cube.fixture = love.physics.newFixture(cube.body,cube.shape)
	cube.fixture:setFriction(.5)
	table.insert(self.geo,cube)
end
world.bodies = {}

function world:createCube(w,h,x,y,_color)
	local cube = {}
	cube.color = _color
	cube.body = love.physics.newBody(self.world,x,y,"dynamic")
	cube.shape = love.physics.newRectangleShape(30,30)
	cube.fixture = love.physics.newFixture(cube.body,cube.shape,1)
	cube.fixture:setRestitution(.2)
	cube.fixture:setFriction(.6)
	cube.body:setMass(30)
	table.insert(world.bodies,cube)
end

function world:draw()
	love.graphics.clear(unpack(colors.bg))
	love.graphics.setColor(unpack(colors.black))
	for i,floor in ipairs(self.geo) do
		love.graphics.polygon("fill",floor.body:getWorldPoints(floor.shape:getPoints()))
	end
	for i,cube in ipairs(self.bodies) do
		love.graphics.setColor(unpack(cube.color))
		love.graphics.polygon("fill",cube.body:getWorldPoints(cube.shape:getPoints()))
	end
	love.graphics.setColor(unpack(colors.black))
	love.graphics.print("cubes: " .. tostring(#self.bodies),bit8,10,10)
end

function world:deleteCube()
	for i,cube in ipairs(self.bodies) do
		if cube.body:getY() > 1200 then
			cube.fixture:destroy()
			table.remove(self.bodies,i)
		end
	end
end
return world
