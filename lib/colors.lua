local colors = {
	startbg = {.7,.7,.7,.4},
	bg = {1,1,1,1},
	black = {0,0,0,1},
	white = {1,1,1,1},
	red = {.9,.4,.3,1},
	blue = {.3,.9,.4,1}
}
colors.btn = colors.black
colors.btnhover = colors.white
colors.text = colors.white
colors.texthover = colors.black
return colors
