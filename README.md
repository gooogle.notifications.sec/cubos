# Cubos

![logo](https://64.media.tumblr.com/9a9e3d86ad61c3dedb2b132e09fe25db/950c70f2c041613d-6d/s250x400/53f2530be6ea5f78a2d7a53f12c54b492373e427.png)

This is a Lua LOVE2D framework test game, it has some concepts about drawing bodies in world and detect some events during the execution. The code is 
not made to be scalable.
I hope you can enjoy with this example and modify if you want to test some LOVE2D features. 

### Dependencies

```
Lua 5.3

LOVE 11.3 (Mysterious Mysteries)
```

### Usage 

clone this repo and execute:

**In Linux and windows**
```
git clone  git@gitlab.com:gooogle.notifications.sec/cubos.git
love cubos
```
**note:** _in windows use powershell or cmd_


