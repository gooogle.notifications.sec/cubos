local screen = {
	x = 500,
	y = 500,
}

local colors = require "lib.colors"
local world = require "lib.world"
local start = require "lib.start"

local font = nil

function love.load()
	font = love.graphics.newFont("fonts/8bit.ttf",15)
	love.graphics.setBackgroundColor(colors.bg)
	love.window.setMode(screen.x,screen.y)
	world:geoLoad(screen.x,screen.y)
end

function love.update(dt)	
	if start.active then
		start.mouseX = love.mouse.getX()
		start.mouseY = love.mouse.getY()
	else	
		world.world:update(dt)
		world:deleteCube()
	end
end

function love.draw()
	--love.graphics.clear(unpack(colors.bg))
	if start.active then
		start:draw1(screen.x,screen.y)
	else
		world:draw()
	end
end

function love.mousepressed(x,y,button)
	if button == 1 then
		if start.active then
			start:mousepressed(x,y,screen.x,screen.y)
		else
			world:createCube(screen.x, screen.y,x,y,colors.red)
		end
	elseif button == 2 then
		if not start.active then
			world:createCube(screen.x, screen.y,x,y,colors.blue)
		end
	end
end

function love.keypressed(key)
	if key == "escape" then
		start.first = false
		start.active = true
	end
end
